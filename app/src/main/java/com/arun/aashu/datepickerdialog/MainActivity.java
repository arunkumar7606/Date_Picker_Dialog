package com.arun.aashu.datepickerdialog;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    EditText b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        b=findViewById(R.id.editText);

                b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar ca=Calendar.getInstance();
                new DatePickerDialog(MainActivity.this,dateListener,
                        ca.get(Calendar.YEAR),ca.get(Calendar.MONTH),
                        ca.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

    }

    DatePickerDialog.OnDateSetListener dateListener=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int Year, int month, int dayofMonth) {
         b.setText(dayofMonth +"/"+month+"/"+Year);


        }
    };
}

